<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JASMIL</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
</head>
<body>
    <div class="container">
        <div class="row justify-content-center pt-5">
            <div class="col-md-6">
                <div class="card shadow">
                    <div class="card-header">
                        <h4>Kesegaran Jasmani Lari 12 menit</h4>
                    </div>
                    <div class="card-body">
                        <div class="mb-3">
                            <label for="umur" class="form-label">Umur :</label>
                            <select class="form-select" name="umur" id="umur">
                                <option value="0">Pilih Umur Anda</option>
                                <option value="18-21">Umur 18-21</option>
                                <option value="22-25">Umur 22-25</option>
                                <option value="26-29">Umur 26-29</option>
                            </select>
                            <!-- <input type="text" name="umur" class="form-control" id="umur" placeholder="Input Umur"> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('select[name=umur]').on('change', function() {
            var umur = document.getElementById("umur").value;
            var jarak = 0;
            var dpm = 0;
            var pace = 400;

            if (umur === "18-21" ) {
                jarak = 3500;                
            }else if(umur === "22-25") {
                jarak = 3300; 
            } else if(umur === "26-29"){                
                jarak = 3100;
            }
            else {
                jarak = 0;
            }

            if (jarak !== 0) {
                dpm = (720/jarak)*pace;
                hasil = dpm.toFixed(2)
                alert(hasil+" detik/"+jarak+" meter adalah waktu dan jarak yang harus anda tempuh untuk mencapai target "+jarak+" Km dengan waktu 12 Menit");
                // console.log(dpm.toFixed(2));
            } else {
                alert('Silakan Pilih Umur');
            }
            // console.log("Hello world!");
        });
    </script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>