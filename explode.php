<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Explode</title>
</head>
<body>
    <h1>Pemecahan String dengan fungsi explode() pada PHP</h1>

    <?php
        $string = "Belajar Pemrograman PHP";

        echo "STRING = ".$string;

        echo "<br><br>";

        $data = explode(" ", $string);

        print_r($data);        

        echo "<br><br>";

        foreach ($data as $key => $value) {
            echo "Data ".$key." = ".$value ."<br>"; 
        }
    ?>
</body>
</html>