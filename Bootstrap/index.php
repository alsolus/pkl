<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
  </head>
  <body>
    <!-- row 1 -->
    <div class="row text-center">
      <div class="col-4 bg-success">
        <h1>halaman kiri</h1>
      </div>
      <div class="col-4 bg-primary">
        <h1>halaman tengah</h1>
      </div>
      <div class="col-4 bg-danger">
        <h1>halaman kanan</h1>          
      </div>
    </div>

    <!-- row 2 -->
    <div class="row text-center">
      <div class="col-4 bg-info">
        <h1>Test</h1>
      </div>
      <div class="col-4 bg-secondary">
        <h1>Test</h1>
      </div>
      <div class="col-4 bg-warning">
        <h1>Test</h1>
      </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
  </body>
</html>