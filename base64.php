<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Base64</title>
</head>
<body>
    <?php
        $string = 'Password';

        $encode = base64_encode($string);

        $decode = base64_decode($encode);

        echo "Hasil Encode = ".$encode."<br><br>";
        echo "Hasil Decode = ".$decode;
    ?>
</body>
</html>