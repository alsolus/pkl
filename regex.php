<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        // This is a single-line comment
        # This is also a single-line comment

        $str = "Saya Suka Belanjar koding";
        $pattern = "/koding/i";
        
        echo preg_replace($pattern, "Pemrograman", $str);
    ?>
</body>
</html>