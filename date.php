<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Date</title>
</head>
<body>
    <?php
        echo date('d-m-y h:i:s a');
        echo "<br>";
        echo date('D-M-Y');
        echo "<br>";
        echo date('d/m/y');
        echo "<br>";
        echo date('D/M/Y');
        echo "<br>";
        echo date('D - M / Y');
        echo "<br>";
        echo date('d / M / y');
        echo "<br>";
        echo date('l, d-m-Y');
        echo "<br>";
        echo date('h:i:s a');
        echo "<br>";

    ?>
</body>
</html>