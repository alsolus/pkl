<?php
$con    = mysqli_connect('localhost', 'root', '');
$db     = mysqli_select_db($con, 'level_up');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CRUD</title>
    <!-- Icon -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">
    <!-- Datatables -->
    <link href="https://cdn.datatables.net/v/bs5/dt-1.13.6/b-2.4.1/b-html5-2.4.1/b-print-2.4.1/datatables.min.css" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>

<body>
    <div class="container justify-content-center">
        <div class="row text-center pt-4">
            <h1>Membuat CRUD dengan PHP, MYSQL & DataTables</h1>
        </div>

        <br>
        <br>

        <!-- Alert -->
        <?php
        if (isset($_GET['pesan'])) {
            $pesan = $_GET['pesan'];
            if ($pesan == 'create') {
                echo 'Data berhasil di Create';
            } elseif ($pesan == 'update') {
                echo 'Data berhasil di Update';
            } elseif ($pesan == 'delete') {
                echo 'Data berhasil di Delete';
            }
        }

        $data = mysqli_query($con, 'SELECT * FROM users');
        ?>
        <br>
        <br>

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <div class="row justify-content-between">
                            <div class="col-md-auto">
                                <i class="bi bi-person-check"></i> Data Users
                            </div>
                            <div class="col-md-auto">
                                <a href="input.php" class="btn btn-primary mb-4"><i class="bi bi-plus-circle"></i>
                                    Add</a>
                            </div>
                            <hr>
                        </div>

                        <div class="table-resposive">
                            <table class="table" id="TableUser">
                                <thead>
                                    <tr>

                                        <th scope="col">No</th>
                                        <th scope="col">Nama</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Alamat</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <?php
                                        foreach ($data as $i) { ?>

                                            <th scope="row"><?php echo $i['id']; ?></th>
                                            <td><?php echo $i['nama']; ?></td>
                                            <td><?php echo $i['email']; ?></td>
                                            <td><?php echo $i['alamat']; ?></td>

                                        <?php
                                        }
                                        ?>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            let table = $('#TableUser').DataTable({
                dom: 'lBfrtip',
                processing: true,
                lengthMenu: [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, 'All'],
                ],
            });
        });
    </script>

    <!-- DataTables -->
    <script src="https://cdn.datatables.net/v/bs5/dt-1.13.6/b-2.4.1/b-html5-2.4.1/b-print-2.4.1/datatables.min.js">
    </script>
    <!-- Boostrap -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
</body>

</html>