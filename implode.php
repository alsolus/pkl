<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Implode</title>
</head>
<body>
    <h1>Pemecahan String dengan fungsi implode() pada PHP</h1>

    <?php
        $array = array("Belajar", "Pemrograman", "PHP");
        
        print_r($array);
        
        echo "<br><br>";
        
        $data = implode(" ", $array);

        echo $data;
    ?>
</body>
</html>