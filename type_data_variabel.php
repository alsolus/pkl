<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
    // String
    echo "String: <br>";
    $x = "Hello world!";
    $y = 'Hello world!';

    echo $x;
    echo "<br>";
    echo $y;

    echo "<br><br>";

    // Integer
    echo "Integer : <br>";
    $x = 5985;
    var_dump($x);

    echo "<br><br>";

    // Float
    echo "Float : <br>";
    $x = 10.365;
    var_dump($x);

    echo "<br><br>";

    // Boolean
    echo "Boolean : <br>";
    echo $x = true;
    echo $y = false;

    echo "<br><br>";

    // array
    echo "Array : <br>";
    $cars = array("Volvo","BMW","Toyota");
    var_dump($cars);

    echo "<br><br>";

    // Objeck
    echo "Objeck : <br>";
    class Car {
    public $color;
    public $model;
    public function __construct($color, $model) {
        $this->color = $color;
        $this->model = $model;
    }
    public function message() {
        return "My car is a " . $this->color . " " . $this->model . "!";
    }
    }
    
    $myCar = new Car("black", "Volvo");
    echo $myCar -> message();
    echo "<br>";
    $myCar = new Car("red", "Toyota");
    echo $myCar -> message();

    echo "<br><br>";

    // Null
    echo "Null : <br>";
    $x = "Hello world!";
    $x = null;
    var_dump($x);
    ?>
</body>
</html>