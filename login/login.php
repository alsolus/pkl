<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <!-- ICON -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
    <?php
    $con = mysqli_connect('localhost', 'root', '');
    $db = mysqli_select_db($con, "level_up");

    $username = $_POST['username'];
    $password = $_POST['password'];

    $query = mysqli_query($con, "select * from admin where username='$username' and password='$password'");
    $cek = mysqli_num_rows($query);
    $hasil = ($cek == 1) ? "LOGIN SUKSES" : "LOGIN GAGAL";

    // echo $hasil;
    ?>

    <div class="container">
        <div class="row justify-content-center pt-5">
            <div class="col-md-6">
            <?php
                if ($cek == 1) {
                    echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
                    <strong>$hasil</strong>
                    <button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'></button>
                    </div>";
                } else {
                    echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>
                    <strong>$hasil</strong>
                    <button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'></button>
                    </div>";
                }        
            ?>
            <a href="index.php" class="btn btn-secondary">Back</a>
            </div>
        </div>
    </div>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>