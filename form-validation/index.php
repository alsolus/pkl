<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form-Validation-php</title>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
    <div class="row p-4">
        <div class="col-md-6">
            <h1>Membuat Form Validation</h1>
            
            <!-- Pengecekan Kondisi -->
            <?php
            if (isset($_GET['nama'])) {
                if ($_GET['nama'] == 'kosong') {
                    echo "<h4 style='color:red'>Nama Belum Di Masukan !</h4>";
                }else{
                    echo "<h4 style='color:red'>Nama Anda adalah ". $_GET['nama'] ."</h4>";
                }
            }
            ?>
            
            <!-- Form -->
            <h4>Masukan Nama Anda :</h4>
            <form action="cek.php" method="post">
                <label for="nama" class="form-label"></label>
                <input type="text" name="nama" class="form-control" required>
                <button type="submit" class="btn btn-primary mt-2">Cek</button>
            </form>
        </div>
    </div>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>